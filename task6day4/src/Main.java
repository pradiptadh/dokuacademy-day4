import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Integer k = 3 , sum = 0;
        List<Integer> arrayList = new ArrayList<>(Arrays.asList(2, 1, 5, 1, 3, 2));
        for (int i = 0; i < k ; i++) {
                sum += arrayList.get(i);
        }

        int curr_sum = sum;
        for (int i=k; i<arrayList.size(); i++)
        {
            curr_sum += arrayList.get(i) - arrayList.get(i-k);
            sum = Math.max(sum, curr_sum);
        }

        System.out.println(sum);
    }
}