import java.util.*;

public class Main {
    public static void main(String[] args) {

        //create pair with sum
        
        Integer target = 4;

        List<Integer> listNumbers = new ArrayList<>(Arrays.asList(1,2,3,4,6));
        List<Integer> result = new ArrayList<>();

        Map<Integer, Integer> hashmap = new HashMap<>();

        for (int i = 0; i < listNumbers.size(); i++) {
            if (hashmap.containsKey(listNumbers.get(i))){
                result.add(hashmap.get(listNumbers.get(i)));
                result.add(i);
            }else{
                Integer diff = target - listNumbers.get(i);
                hashmap.put(diff,i);
            }
        }
        System.out.println(result);
    }
}