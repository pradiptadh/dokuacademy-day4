import java.util.*;

public class Main {
    public static void main(String[] args) {
        String angka ;
        System.out.println("masukkan angka = ");
        Scanner input = new Scanner(System.in);
        angka = input.nextLine();
        List<Integer> newAngka = new ArrayList<>();
        Map<Integer, Integer> result = new HashMap<>();
        for (int i = 0; i <angka.length(); i++) {
            char c = angka.charAt(i);
            Integer number = Integer.valueOf(Character.toString(c));
            newAngka.add(number);

        }

        for (int i = 0; i<newAngka.size(); i++) {
            if (result.containsKey(newAngka.get(i))) {
                result.put(newAngka.get(i), result.get(newAngka.get(i))+1);
            } else {
                result.put(newAngka.get(i), 1);
            }
        }

        List<Integer> answerList = new ArrayList<>();
        for (Integer key: result.keySet()) {
            if (result.get(key) == 1) {
                answerList.add(key);
            }
        }

        System.out.println(answerList);

    }

}