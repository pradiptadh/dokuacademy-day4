import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //pertama add arr1 ke map
        //kedua check melewati arr2 misal dia ada yang sama dengan arr1 di map maka diremove dari mapnya jika engga ya lanjut
        //abis itu buat list baru buat nampung nilai key dari mapnya

        List<Integer> arr1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> arr2 = new ArrayList<>(Arrays.asList(1, 3, 5, 10, 16));
        List<Integer> finalResult = new ArrayList<>();

        HashMap<Integer, Integer> resultMap = new HashMap<>();
        for (int i = 0; i <arr1.size() ; i++) {
                resultMap.put(arr1.get(i) , 1);
        }
        for (int j = 0; j < arr2.size(); j++) {
                if (resultMap.containsKey(arr2.get(j))){
                    resultMap.remove(arr2.get(j));
                }
        }

        for (Integer key : resultMap.keySet()){
            finalResult.add(key);
        }

        System.out.println(finalResult);
    }
}