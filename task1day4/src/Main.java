import java.util.*;

public class Main {
    public static void main(String[] args) {
        //create arraylist merge using hashset

        //arraylist pertama
        List<String> arrayListPertama = new ArrayList<>();
        arrayListPertama.add("kazuya");
        arrayListPertama.add("jin");
        arrayListPertama.add("lee");

        //arraylist kedua
        List<String> arrayListKedua = new ArrayList<>();
        arrayListKedua.add("kazuya");
        arrayListKedua.add("feng");

        //merge arraylist with set
        Set<String> listTekken = new LinkedHashSet<>(arrayListPertama);
        listTekken.addAll(arrayListKedua);
        System.out.println(listTekken);

    }
}